﻿using UnityEngine;
using System.Collections;

public class FlickeringLight : MonoBehaviour {

	public GameObject Light;
	Light testLight;
	public float minWaitTime;
	public float maxWaitTime;
	
	void Start () {
        GameObject lightGameObject = new GameObject("The Light");
		Light testLight = lightGameObject.AddComponent<Light>();
		StartCoroutine(Flashing());
	}
	  
	IEnumerator Flashing ()
	{
		while (true)
		{
			yield return new WaitForSeconds(Random.Range(minWaitTime,maxWaitTime));
			testLight.enabled = ! testLight.enabled;

		}
	}
}