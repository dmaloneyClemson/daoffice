﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Flash : MonoBehaviour
{
    // Start is called before the first frame update
    public float flashTimelength = .2f;
    public bool doCameraFlash = false;

    private Image flashImage;
    private float startTime;
    private bool flashing = false;
    float timer;
    int waitingTime  = 5;

    void Start()
    {
        flashImage = GetComponent<Image>();
        Color col = flashImage.color;
        col.a = 0.0f;
        flashImage.color = col;

    }

    // Update is called once per frame
    void Update()
    {
        timer+= Time.deltaTime;
        if(timer > waitingTime){
            doCameraFlash = true;
            timer = 1;
        }
    
        if(doCameraFlash && !flashing)
        {
            CameraFlash();
        }
        else
        {
            doCameraFlash = false;
        }
        
    }
    public void CameraFlash()
        {
            Color col = flashImage.color;

            startTime = Time.time;

            doCameraFlash = false;

            col.a = 1.0f;

            flashImage.color = col;

            flashing = true;

            StartCoroutine(FlashCoroutine());
        }

        IEnumerator FlashCoroutine ()
        {
            bool done = false;

            while(!done)
            {
                float perc; 
                Color col = flashImage.color;

                perc = Time.time - startTime;
                perc = perc / flashTimelength;

                if(perc>1.0f)
                {
                    perc = 1.0f;
                    done = true;
                }

                col.a = Mathf.Lerp(1.0f,0.0f,perc);

                flashImage.color = col;
                flashing = true;

                yield return null;
            }
            flashing = false;

            yield break;
        }
}
