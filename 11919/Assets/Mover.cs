﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour {

	public AudioSource voicePlayback;
	// Use this for initialization
	void Start () {
		voicePlayback = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	private void Update () {

		transform.position += transform.forward * Time.deltaTime;
	
	}
	
	
	
}
