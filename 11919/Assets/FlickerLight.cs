﻿using UnityEngine;
using System.Collections;
 
public class FlickerLight : MonoBehaviour {
 
    public GameObject Lights;
    public float timer;
 
    void Start()
    {
        StartCoroutine(FlickeringLight());
    }
 
    IEnumerator FlickeringLight()
    {
        Lights.SetActive(false);
        yield return new WaitForSeconds(1);
        Lights.SetActive(true);
        yield return new WaitForSeconds(7);
        StartCoroutine(FlickeringLight());
    }
}