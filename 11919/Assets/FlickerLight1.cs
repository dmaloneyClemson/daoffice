﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlickerLight1 : MonoBehaviour {
 
    public GameObject Lights;
    public float timer;
 
    void Start()
    {
        StartCoroutine(FlickeringLight());
    }
 
    IEnumerator FlickeringLight()
    {
        Lights.SetActive(false);
        yield return new WaitForSeconds(timer);
        Lights.SetActive(true);
        yield return new WaitForSeconds(timer);
        StartCoroutine(FlickeringLight());
    }
}